package tn.esprit.examen;

import java.text.ParseException;

import javax.naming.NamingException;

public class Main {

	public static void main(String[] args) throws NamingException, ParseException {
		AjouterCategorie.main(args);
		AjouterArticle.main(args);
		AjouterCommande.main(args);
		AffecterArticleCategorie.main(args);
		AffecterArticleACommande.main(args);
	}

}
