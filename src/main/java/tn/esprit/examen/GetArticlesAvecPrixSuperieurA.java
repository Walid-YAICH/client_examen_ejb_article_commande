package tn.esprit.examen;

import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.entites.Article;
import tn.esprit.service.IServicesRemote;

public class GetArticlesAvecPrixSuperieurA {

	public static void main(String[] args) throws NamingException {
		String jndiName = "examenThouraya-ear/examenThouraya-ejb/Services!tn.esprit.service.IServicesRemote";
		Context context = new InitialContext();
		IServicesRemote servicesRemote = (IServicesRemote) context.lookup(jndiName);
		
		List<Article> articles = servicesRemote.getArticlesAvecPrixSuperieurA(200);
		articles.forEach(System.out::println);
	}	
}
