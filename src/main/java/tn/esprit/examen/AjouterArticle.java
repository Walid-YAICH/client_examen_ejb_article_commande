package tn.esprit.examen;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.entites.Article;
import tn.esprit.service.IServicesRemote;

public class AjouterArticle {

	public static void main(String[] args) throws NamingException {
		String jndiName = "examenThouraya-ear/examenThouraya-ejb/Services!tn.esprit.service.IServicesRemote";
		Context context = new InitialContext();
		IServicesRemote servicesRemote = (IServicesRemote) context.lookup(jndiName);
		
		Article pain = new Article("Pain", 300, "img1.png");
		
		servicesRemote.ajouterArticle(pain);
	}
	
}
