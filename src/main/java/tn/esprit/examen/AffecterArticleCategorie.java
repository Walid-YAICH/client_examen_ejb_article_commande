package tn.esprit.examen;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.service.IServicesRemote;

public class AffecterArticleCategorie {

	public static void main(String[] args) throws NamingException {
		String jndiName = "examenThouraya-ear/examenThouraya-ejb/Services!tn.esprit.service.IServicesRemote";
		Context context = new InitialContext();
		IServicesRemote servicesRemote = (IServicesRemote) context.lookup(jndiName);
		
		servicesRemote.affecterArtCat(1, 1);
	}	
}
