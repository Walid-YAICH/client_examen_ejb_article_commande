package tn.esprit.examen;

import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.service.IServicesRemote;

public class GetAllArticleNamesByCategorie {

	public static void main(String[] args) throws NamingException {
		String jndiName = "examenThouraya-ear/examenThouraya-ejb/Services!tn.esprit.service.IServicesRemote";
		Context context = new InitialContext();
		IServicesRemote servicesRemote = (IServicesRemote) context.lookup(jndiName);
		
		List<String> articleNames = servicesRemote.getAllArticleNamesByCategorie(1);
		articleNames.forEach(System.out::println);
	}	
}
