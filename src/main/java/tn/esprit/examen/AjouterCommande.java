package tn.esprit.examen;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.entites.Commande;
import tn.esprit.entites.Status;
import tn.esprit.service.IServicesRemote;

public class AjouterCommande {

	public static void main(String[] args) throws NamingException, ParseException {
		String jndiName = "examenThouraya-ear/examenThouraya-ejb/Services!tn.esprit.service.IServicesRemote";
		Context context = new InitialContext();
		IServicesRemote servicesRemote = (IServicesRemote) context.lookup(jndiName);

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Commande commande = new Commande(dateFormat.parse("28/11/2018"), Status.PRETPOURLIVRAISON);
		
		servicesRemote.ajouterCommande(commande);
	}
	
}
